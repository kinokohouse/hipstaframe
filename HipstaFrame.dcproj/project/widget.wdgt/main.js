// HipstaFrame 2.1
//
// 2.1 contains a fix for changed RegEx.exec behaviour in macOS 10.13.
// 2.0 adds adjustable frame size (180, 270 and 360) and support for all macOS versions that support Dashboard
// (Versions 10.4.3 through 10.9 work a little different than the ones above that drag-and-drop-wise).


// constants
var animationValues = [[97,25,8],[112,24],[23,24],[152,32,8],[167,30],[28,28],[230,38,10],[228,115],[34,34]]
var OSVersion = 0;
var magicKey = 93; // right apple
var debug = true;

// variables
var previousImage = "";
var OSFix = 10
var sizeChoice = 1;
var animation = {duration:1, steps:5, count:5, timestep:0, currentsize:0, endsize:0, sizestep:0.0, texttop:0, textleft:0, textsize:0, textnewtop:0, textnewleft:0, textnewsize:0, texttopstep:0, textleftstep:0, textsizestep:0, popuptop:0, popupleft:0, popupnewtop:0, popupnewleft:0, popuptopstep:0, popupleftstep:0, buttonbottom:0, buttonright:0, buttonnewbottom:0, buttonnewright:0, buttonbottomstep:0, buttonrightstep:0, timer:null};
var fixFlag = false;
var fixTarget = false;

// event listeners
document.addEventListener("keydown",checkForDown,true);
document.addEventListener("keyup",checkForUp,true);

// standard routines
function load() {
    dashcode.setupParts();
    OSVersion = getOSVersion();
    getPrefs();
}

function remove() {
    killPrefs();
}

function hide() {
}

function show() {
}

function sync() {
}

function showBack(event) {
    var front = document.getElementById("front");
    var back = document.getElementById("back");
    if (window.widget) 
    {
        widget.prepareForTransition("ToBack");
    }
    front.style.display = "none";
    back.style.display = "block";
    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event) {
    var front = document.getElementById("front");
    var back = document.getElementById("back");
    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }
    front.style.display="block";
    back.style.display="none";
    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

// custom string function
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
}

// drag & drop related
function dragDrop(event) {
    if (OSVersion >= OSFix) {
        event.stopPropagation();
        event.preventDefault();
        try {
            var files = event.dataTransfer.files;
            var count = files.length;
            if (count > 0) {  
                var file = files[0];
                var n = file.name;
                if (n.endsWith(".jpeg") || n.endsWith(".JPEG") || n.endsWith(".jpg") || n.endsWith(".JPG") || n.endsWith(".png") || n.endsWith(".PNG")) {
                    var reader = new FileReader();
                        reader.onloadend = pictureDone;
                        reader.readAsDataURL(file);
                }
            }
        }
        catch(event) {
            // alert ("Problems with reading image!");
        }
    }
    else {
        try {
            var uriString = event.dataTransfer.getData("text/uri-list");	
            var uriList = uriString.split("\n");
            v = uriList[0];
            if (v.endsWith(".jpeg") || v.endsWith(".JPEG") || v.endsWith(".jpg") || v.endsWith(".JPG") || v.endsWith(".png") || v.endsWith(".PNG")) {
                document.picture.src = v;
            }
        } 
        catch (e) {
            // alert("Problem fetching URI: " + e);
        }
        event.stopPropagation();
        event.preventDefault();
    }
    setPrefs();
}
    
function dragEnter(event) {
    previousImage = document.picture.src;
	dragDrop(event);
}

function dragOver(event) {
	event.stopPropagation();
	event.preventDefault();
}

function dragLeave(event) {
    document.picture.src = previousImage;
	event.stopPropagation();
	event.preventDefault();
    setPrefs();
}

function pictureDone(event) {
    v = event.target.result;
    document.picture.src = v;    
}

// preferences
function getPrefs() {
    if (window.widget) {
        if (widget.preferenceForKey(widget.identifier + "-" + "currentImage") === undefined) {
            previousImage = "Images/default-photo.jpg";
            document.picture.src = previousImage;
            widget.setPreferenceForKey(document.picture.src, widget.identifier + "-" + "currentImage");
        }
        else {
            previousImage = document.picture.src;
            document.picture.src = widget.preferenceForKey(widget.identifier + "-" + "currentImage");
        }
        if (widget.preferenceForKey(widget.identifier + "-" + "widgetSize") === undefined) {
            widget.setPreferenceForKey(sizeChoice, widget.identifier + "-" + "widgetSize");
        }
        else {
            sizeChoice = widget.preferenceForKey(widget.identifier + "-" + "widgetSize");
        }
        if (widget.preferenceForKey("OSFix") === undefined) {
            widget.setPreferenceForKey(OSFix, "OSFix");
        }
        else {
            OSFix = widget.preferenceForKey("OSFix");        
        }
    }
    popup.options.selectedIndex = (sizeChoice - 1);
    handleSizeChoice();
}

function setPrefs() {
    if (window.widget) {
        widget.setPreferenceForKey(document.picture.src, widget.identifier + "-" + "currentImage");
        widget.setPreferenceForKey(sizeChoice, widget.identifier + "-" + "widgetSize");
        widget.setPreferenceForKey(OSFix, "OSFix");
    }
}

function setPrefsLite() {
    if (window.widget) {
        widget.setPreferenceForKey(sizeChoice, widget.identifier + "-" + "widgetSize");
        widget.setPreferenceForKey(OSFix, "OSFix");
    }
}

function killPrefs() {
    if (window.widget) {
        widget.setPreferenceForKey(null, widget.identifier + "-" + "currentImage");
        widget.setPreferenceForKey(null, widget.identifier + "-" + "widgetSize");
    }
}

function handleSizeChoice() {
    widgetsize.style.top = animationValues[(sizeChoice - 1) * 3][0] + "px";
    widgetsize.style.left = animationValues[(sizeChoice - 1) * 3][1] + "px";
    widgetsize.style.fontSize = animationValues[(sizeChoice - 1) * 3][2] + "pt";
    
    popup.style.top = animationValues[((sizeChoice - 1) * 3) + 1][0] + "px";
    popup.style.left = animationValues[((sizeChoice - 1) * 3) + 1][1] + "px";
    
    done.style.bottom = animationValues[((sizeChoice - 1) * 3) + 2][0] + "px";
    done.style.right = animationValues[((sizeChoice - 1) * 3) + 2][1] + "px";

    copyright.style.bottom = animationValues[((sizeChoice - 1) * 3) + 2][0] + "px";
    copyright.style.left = animationValues[(sizeChoice - 1) * 3][1] + "px";

    calculatedSize = sizeChoice * 90 + 90;
    
    if (window.widget) {
        window.resizeTo(calculatedSize + 24, calculatedSize + 24);
    }
    
    picture.style.width = calculatedSize + "px";
    picture.style.height = calculatedSize + "px";
    image.style.width = calculatedSize + "px";
    image.style.height = calculatedSize + "px";
    backdrop.style.width = (calculatedSize + 23) + "px";
    backdrop.style.height = (calculatedSize + 23) + "px";
    backimg.style.width = (calculatedSize + 23) + "px";
    backimg.style.height = (calculatedSize + 23) + "px";
    front.style.width = (calculatedSize + 23) + "px";
    front.style.height = (calculatedSize + 23) + "px";

    adjustShadow();
}

// popup button
function receivedPopupChange(event) {
    previousSizeIndex = sizeChoice;
    sizeChoice = parseInt(popup.options[popup.options.selectedIndex].value,10);
    newSizeIndex = sizeChoice;
    startAnimation(previousSizeIndex, newSizeIndex);
    setPrefsLite();
}

// animation
function startAnimation(previousSizeIndex, newSizeIndex) {
    if (previousSizeIndex == newSizeIndex) return;
    fromSize = previousSizeIndex * 90 + 90;
    toSize = newSizeIndex * 90 + 90;
    animation.count = animation.steps;

    // window
    animation.currentsize = fromSize;
    animation.sizestep = (toSize - fromSize) / animation.steps;
    animation.timestep = animation.duration / animation.steps;
    animation.currentsize = fromSize + 24;
    animation.endsize = toSize + 24;
    backdrop.style.width = 1;
    backdrop.style.height = 1;
    
    // label
    animation.texttop = animationValues[(previousSizeIndex - 1) * 3][0];
    animation.textleft = animationValues[(previousSizeIndex - 1) * 3][1];
    animation.textsize = animationValues[(previousSizeIndex - 1) * 3][2];
    animation.textnewtop = animationValues[(newSizeIndex - 1) * 3][0];
    animation.textnewleft = animationValues[(newSizeIndex - 1) * 3][1];
    animation.textnewsize = animationValues[(newSizeIndex - 1) * 3][2];
    animation.texttopstep = (animation.textnewtop - animation.texttop) / animation.steps;
    animation.textleftstep = (animation.textnewleft - animation.textleft) / animation.steps;
    animation.textsizestep = (animation.textnewsize - animation.textsize) / animation.steps;
    
    // popup
    animation.popuptop = animationValues[((previousSizeIndex - 1) * 3) + 1][0];
    animation.popupleft = animationValues[((previousSizeIndex - 1) * 3) + 1][1];
    animation.popupnewtop = animationValues[((newSizeIndex - 1) * 3) + 1][0];
    animation.popupnewleft = animationValues[((newSizeIndex - 1) * 3) + 1][1];
    animation.popuptopstep = (animation.popupnewtop - animation.popuptop) / animation.steps;
    animation.popupleftstep = (animation.popupnewleft - animation.popupleft) / animation.steps;
    
    // button
    animation.buttonbottom = animationValues[((previousSizeIndex - 1) * 3) + 2][0];
    animation.buttonright = animationValues[((previousSizeIndex - 1) * 3) + 2][1];
    animation.buttonnewbottom = animationValues[((newSizeIndex - 1) * 3) + 2][0];
    animation.buttonnewright = animationValues[((newSizeIndex - 1) * 3) + 2][1];
    animation.buttonbottomstep = (animation.buttonnewbottom - animation.buttonbottom) / animation.steps;
    animation.buttonrightstep = (animation.buttonnewright - animation.buttonright) / animation.steps;
    
    //copyright
    
    if (animation.timer !== null) {
        clearTimeout(animation.timer);
        animation.timer = null;
    }
    animation.timer = setTimeout('stepAnimation()', 100);
}

function stepAnimation() {
    var newsize = animation.currentsize + animation.sizestep;
    
    var newtexttop = animation.texttop + animation.texttopstep;
    var newtextleft = animation.textleft + animation.textleftstep;
    var newtextsize = animation.textsize + animation.textsizestep;
    
    var newpopuptop = animation.popuptop + animation.popuptopstep;
    var newpopupleft = animation.popupleft + animation.popupleftstep;
    
    var newbuttonbottom = animation.buttonbottom + animation.buttonbottomstep;
    var newbuttonright = animation.buttonright + animation.buttonrightstep;
        
    if (window.widget) {
        window.resizeTo(newsize, newsize);
    }
    
    image.style.width = (newsize - 24) + "px";
    image.style.height = (newsize - 24) + "px";
    
    widgetsize.style.top = newtexttop + "px";
    widgetsize.style.left = newtextleft + "px";
    widgetsize.style.fontSize = newtextsize + "pt";
    
    popup.style.top = newpopuptop + "px";
    popup.style.left = newpopupleft + "px";
    
    done.style.bottom = newbuttonbottom + "px";
    done.style.right = newbuttonright + "px";
    
    copyright.style.bottom = newbuttonbottom + "px";
    copyright.style.left = newtextleft + "px";
    
    animation.currentsize = newsize;

    animation.textleft = newtextleft;
    animation.texttop = newtexttop;
    animation.textsize = newtextsize;
    
    animation.popupleft = newpopupleft;
    animation.popuptop = newpopuptop;
    
    animation.buttonbottom = newbuttonbottom;
    animation.buttonright = newbuttonright;

    animation.count--;
    if (animation.count == 0) {
        animation.currentsize = animation.endsize;
        animation.count = animation.steps;
        if (animation.timer !== null) {
            clearTimeout(animation.timer);
            animation.timer = null;
        }
        image.style.width = (newsize - 24) + "px";
        image.style.height = (newsize - 24) + "px";
        picture.style.width = (newsize - 24) + "px";
        picture.style.height = (newsize - 24) + "px";
        backimg.style.width = (newsize - 1) + "px";
        backimg.style.height = (newsize - 1) + "px";
        backdrop.style.width = (newsize - 1) + "px";
        backdrop.style.height = (newsize - 1) + "px";
        front.style.width = (newsize - 1) + "px";
        front.style.height = (newsize - 1) + "px";
        adjustShadow();
        return;
    }
    animation.timer = setTimeout('stepAnimation()', animation.timestep);
}

function adjustShadow() {
    switch(sizeChoice) {
        case 1:
            backdrop.style.top = "0px";
            backdrop.style.left = "0px";
            break;
        case 2:
            backdrop.style.top = "0px";
            backdrop.style.left = "0px";
            break;
        case 3:
            backdrop.style.top = "0px";
            backdrop.style.left = "0px";
    }
}

// back button event handler
function handleFixStuffIfNeedBe(event) {
    if (!fixFlag) {
        showFront(event);
    }
    else {
        if (fixTarget && OSVersion == 9) {
            OSFix = 9;
        }
        else {
            OSFix = 10;
        }
        setPrefsLite();
        showFront(event);
    }
}

// general utilities
function getOSVersion() {
    var vs = navigator.appVersion;
    var rx = vs.match(/10_(\d{1,2})/i);
    return parseInt(rx[1]);
}

// event listener utilities
function checkForDown(event) {
    if (OSVersion !== 9) return;
    var e = event.keyCode;
    if (e == magicKey)
    {
        fixFlag = true;
        if (OSFix !== 9) 
        {
            document.getElementById("done").object.textElement.innerText = "Fix";
            fixTarget = true;
        }
        else 
        {
            document.getElementById("done").object.textElement.innerText = "No Fix";
            fixTarget = false;
        }
    }
}

function checkForUp(event) {
    fixFlag = false;
    document.getElementById("done").object.textElement.innerText = "Ready";
}

