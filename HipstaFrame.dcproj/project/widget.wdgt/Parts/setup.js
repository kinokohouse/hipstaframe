/* 
 This file was generated by Dashcode and is covered by the 
 license.txt included in the project.  You may edit this file, 
 however it is recommended to first turn off the Dashcode 
 code generator otherwise the changes will be lost.
 */
var dashcodePartSpecs = {
    "copyright": { "text": "© 2012-2017 Kinoko House. MIT License.", "view": "DC.Text" },
    "done": { "creationFunction": "CreateGlassButton", "onclick": "handleFixStuffIfNeedBe", "text": "Ready" },
    "image": { "view": "DC.ImageLayout" },
    "info": { "backgroundStyle": "black", "creationFunction": "CreateInfoButton", "foregroundStyle": "white", "frontID": "front", "onclick": "showBack" },
    "picture": { "view": "DC.ImageLayout" },
    "widgetsize": { "text": "Widget size:", "view": "DC.Text" }
};













