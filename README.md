# HipstaFrame #
Dashboard widget for macOS to show a square photo on your dashboard, like the ones you would take with Hipstamatic. Source available under an MIT license. Made in DashCode. Ready to use binary available from downloads section. Tested on 10.6.8 and above, should work on systems as early as 10.4.3 (and now also verified to work on 10.13 after the last fix).


Building
--------
Editing and deploying can be done from either version of DashCode, or you can edit the files within the widget bundle with your favourite text/image editors.

Usage
-----
When deployed, drag and drop a picture from any file source (PNG or JPEG). Switch to the back to change widget size (choice of small 180x180, middle 270x270 and large 360x360). Everything you throw into it turns square, so watch out.

Note
----
If you are on 10.9 and have trouble getting the widget to work properly, switch to the back of the widget and hold the RIGHT COMMAND key. The button text should change to 'Fix ON' (no use trying this if you're not on 10.9 BTW, it won't work). Keep the key held while clicking the button, and drag and drop should work again. This setting is global for all instances of the widget (you may have to restart the dock for instances already present) and will persist over restarts. To turn the fix off (don't know why you would want to do that, but hey) repeat the above procedure. I'd love it if you'd contact me if it works for you (or not), so I can permanently fix it.

Image credits
-------------
Default image and icon by James Bold (StockSnap, CC0), image on back by Pawel Kadysz (Tookapic, CC0).

Version info
------------

Current version: 2.1. 